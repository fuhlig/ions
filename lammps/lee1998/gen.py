#!/usr/bin/env python

import sys
from ase import Atom, Atoms

if len(sys.argv) == 1:
    mix = 'arithmetic'
elif len(sys.argv) == 2:
    mix = sys.argv[1]
else:
    print "Please provide zero or only one argument to the function, the mixing rule for the sigmas (arithmetic or geometric)"
    sys.exit(1)

f = open('plain_ions.txt', 'r')

template = """# ION Lee JPCB 102, 4193 (1998) (kJ/mol, A, deg)

ATOMS
#   type  m/uma    q/e    pot   pars
ION  ION   MASS    CHARGE lj    SIGMA  EPSILON

BONDS
# i j    pot    re/A    ka/kJmol-1

ANGLES
# i  j  k    pot    th/deg   ka/kjmol-1
"""

zmat_template = """ION

 ION

ION.ff
"""

spce_sigma = 3.1655
spce_epsilon = 0.6503

for line in f.readlines():
    name, sigma, epsilon, charge = line.split()

    if mix == 'arithmetic':
        sigma = 2 * float(sigma) - float(spce_sigma)
    elif mix == 'geometric':
        sigma = float(sigma)**2 / spce_sigma

    epsilon = float(epsilon)**2 / spce_epsilon

    atoms = Atoms()
    atoms += Atom(symbol=name, position=[0., 0., 0.])
    mass = atoms.get_masses()[0]

    txt = template.replace('ION', name).replace('MASS', str(mass)).replace('CHARGE', charge).replace('EPSILON', str(epsilon)).replace('SIGMA', str(sigma))

    nf = open(name + '.ff', 'w')
    nf.writelines(txt)
    nf.close()

    txt = zmat_template.replace('ION', name)
    nz = open(name + '.zmat', 'w')
    nz.writelines(txt)
    nz.close()
