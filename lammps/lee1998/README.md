
force field parameter for ions and spc/e water taken from: Lee 1998 J. Phys. Chem. B 1998, 102, 4193-4204. Those are actually mainly Dang's ions (see references), except for the Calcium.
It is to be noted the sigma given in the below table is the sigma ion-water (not the sigma for the ion), equivalent for epsilon

to convert to useful formats use gen.py arithmetic/geometric to obtain force field files that can be used to prepare lammps simulations using fftool (a. padua) (this require that ASE is installed)
the force field files provided here are for arithmetic combination rules

ion   | sigma (Angstrom) |  epsilon (kj/mol) |  charge |  reference
------#------------------#-------------------#---------# 
F-    |      3.143       |       0.6998      |    -1   |    17     (a) Dang, L. X. Chem. Phys. Lett. 1992, 200, 21. (b) Dang, L. X.; Garrett, B. C. J. Chem. Phys. 1993, 99, 2972.
Cl-   |      3.785       |       0.5216      |    -1   |    19     Dang, L. X. Chem. Phys. Lett. 1994, 227, 211.
Br-   |      3.854       |       0.5216      |    -1   |    23     Dang, L. X. J. Chem. Phys. 1992, 96, 6970.
I-    |      4.168       |       0.5216      |    -1   |    18     Smith, D. E.; Dang, L. X. J. Chem. Phys. 1994, 100, 3757.
Li+   |      2.337       |       0.6700      |    +1   |    24 b   Dang, L. X. J. Chem. Phys. 1995, 102, 3483.
Na+   |      2.876       |       0.5216      |    +1   |    22     Dang, L. X. Private communication.
K+    |      3.250       |       0.5216      |    +1   |    22     Dang, L. X. Private communication.
Rb+   |      3.348       |       0.5216      |    +1   |    22     Dang, L. X. Private communication.
Cs+   |      3.526       |       0.5216      |    +1   |    20, 21 Dang, L. X.; Kollman, P. A. J. Phys. Chem. 1995, 99, 55., Dang, L. X. J. Am. Chem. Soc. 1995, 117, 6954.
Ca2+  |      3.019       |       0.5216      |    +2   |    25     Steinhauser O. Mol. Phys. 1982, 45, 335; 1982, 46, 827.

b)  The Li+ parameters are for the revised polarizability model (RPOL)

reaction-field electrostatics
electrostatics cut off after half box length (9.32 Angstrom) (9 Angstrom is cutoff for SPC/E model)

SPC/E water model

water density fixed to 0.997 g/cm^3
215 water molecules and one ion
