DESCRIPTION:

Due to the large variety of ions force fields available in the literature used in MD simulations,
we have decided to make a collection of them so they can be properly used and referenced.

Here you will find the topology of many ions force fieldspublished in the literature
ready to be used in you projects. As many results seem to be very dependent in the
ions used please ensure to especify in your publications which one you used. Enjoy!!

Subscribe to the repository if you want to receive warnings when new ions are available.


GROMACS
---------

* For gromacs add the following lines in your top file when your Van der Waals parameters use
sigma epsilon definitions, e.g. Amber, Charmm , OPLSaa, etc..


#include <./top/ions_nb_sigma_eps_gromacs.itp> ; This file MUST be included before any molecule definition.
#include <./top/ions_sigma_eps_gromacs.itp>



